HTML Base
=========

Scaffold for the HTML templates and other projects. Initial folder structure with Gruntfile and tooling prepared for development.

Grunt tasks
-----------

* [load-grunt-tasks](https://www.npmjs.org/package/load-grunt-tasks) - loads the dependencies listed in the `package.json` file.
* [watch + liverealod](https://npmjs.org/package/grunt-contrib-watch) - watch the file, run the task when the files change and trigger the autoreload in the browser.
* [compass](https://github.com/gruntjs/grunt-contrib-compass) - used for compiling the SCSS/SASS files to CSS.
* [autoprefixer](https://npmjs.org/package/grunt-autoprefixer) - the up-to-date smart prefixing utility. No need to write the prefixed CSS3 properties.
* [assemble](https://npmjs.org/package/assemble) - solves the nightmare of the static HTML files. Simple templating engine using HandlebarsJS. With partials, data, helpers etc.
* [imagemin](https://npmjs.org/package/grunt-contrib-imagemin) - losselessly optimizes the images for the production.
* [newer](https://github.com/tschaub/grunt-newer) - speeds up the building process by running tasks only on the files that actually changed.
* [concurrent](https://npmjs.org/package/grunt-concurrent) - used when multiple blocking watch tasks run at the same time. For `watch` and `compass`.
* [usemin](https://npmjs.org/package/grunt-usemin) with:
	* [concat](https://github.com/gruntjs/grunt-contrib-concat) - concatenates multiple JS/CSS files into a single file.
	* [cssmin](https://github.com/gruntjs/grunt-contrib-cssmin) - minifies the CSS files.
	* [rev](https://github.com/cbas/grunt-rev) - prepends the hash to the files for caching bust.
* [requirejs](https://npmjs.org/package/grunt-contrib-requirejs) with [almond](https://github.com/jrburke/almond) for `build/` and minified using [UglifyJS2](https://github.com/mishoo/UglifyJS2) - loads the AMD modules, optimizes them (concatenating, minifying), concatenates and saves the output.
* [clean](https://github.com/gruntjs/grunt-contrib-clean) - cleans the directories, we love to have everything tidy.
* [htmlmin](https://github.com/gruntjs/grunt-contrib-htmlmin) ×

Note: tasks marked with `×` are not implemented yet, on TODO.

In addition, some frontend libraries/polyfills are also included in the `bower.json` file, for example:

* [fastclick](https://github.com/ftlabs/fastclick) - easy-to-use library for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers.